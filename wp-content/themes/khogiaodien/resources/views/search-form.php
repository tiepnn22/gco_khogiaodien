<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="forms search-form">
    <div class="form-content">
        <div class="form-group">
            <input type="text" name="s" required="required" placeholder="Nhập ngành kinh doanh của bạn" class="form-control" value="<?php echo get_search_query(); ?>">
        </div>
        <div class="form-group form-button">
            <button type="submit" class="btn">
                <i class="icofont-search-1"></i>
            </button>
        </div>
    </div>    
</form>