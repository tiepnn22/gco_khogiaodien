<?php
	$post_id 			= get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),300,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="item">
	<div class="content">
		<div class="content-image">
			<div class="image">
                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                    <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                </a>
			</div>
		</div>
		<div class="content-box">
			<h4 class="content-name">
                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                	<?php echo $post_title; ?>
                </a>
			</h4>
			<?php echo show_price_old_price($post_id); ?>
		</div>
		<div class="content-button">
			<?php echo show_add_to_cart_button_ajax($post_id); ?>
		</div>
	</div>
</div>