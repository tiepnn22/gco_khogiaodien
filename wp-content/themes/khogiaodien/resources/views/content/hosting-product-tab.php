<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= get_the_excerpt($post_id);
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);

    //woo
    $donvi 			= get_woocommerce_currency_symbol();	// donvi
    $product_info	= wc_get_product($post_id);
    $type       	= $product_info->is_type( 'variable' ); // check variable
    $info_cart		= WC()->cart->cart_contents;	// info cart
    $cart_count     = WC()->cart->get_cart_contents_count();

    //field
    $product_buytheme_free_hosting = get_field('product_buytheme_free_hosting');

	// Nếu có hosting free, và giỏ có hàng
	if($product_buytheme_free_hosting == 1 && $cart_count > 0) {
        foreach ($info_cart as $info_cart_kq) {
        	$cart_product_id	= $info_cart_kq["product_id"];
        	// Lấy danh mục đầu tiên của sp đó, check xem có thuộc hosting_cat ko ?
        	$check_term_of_product = get_the_terms( $cart_product_id, 'hosting_cat' )[0]->term_id;
			if(empty( $check_term_of_product )) {	// nếu ko có thì là theme, ko phải hosting
				$check_product_theme = "p-theme";
			}
    	}
	}
?>

<div class="item">
	<div class="content">
		<h3><?php echo $post_title; ?></h3>

		<?php
			if($type == true) {
				$variations_info = $product_info->get_available_variations();

				$product_hosting_price_oneyear = $variations_info[0]["display_price"];
				$product_hosting_price = $product_hosting_price_oneyear / 12;
		?>
				<p class="price">
					<?php if(!empty( $product_hosting_price )) { ?>

						<!--Nếu có Theme trong giỏ thì cho mua hosting FREE-->
						<?php if(empty( $check_product_theme )) { ?>
							<span><?php echo format_price($product_hosting_price); ?></span> <?php echo $donvi; ?>/tháng
						<?php } else { ?>
							<span class="d-block">FREE</span>
							<del><span><?php echo format_price($product_hosting_price); ?></span> <?php echo $donvi; ?>/tháng</del>
						<?php } ?>

					<?php } ?>
				</p>
		<?php } ?>
		
		<?php echo $post_excerpt; ?>

		<!-- <div class="button-box btn-time">
			<a href="javascript:void(0)" title="" class="btn-popup" data_popup="hosting-standarrd">
				Chọn thời gian
				<i class="icofont-calendar icon"></i>
			</a>
		</div> -->
		<div class="button-box">
			<a href="javascript:void(0)" title="" class="btn-popup btn" data_popup="hosting-standarrd-<?php echo $post_id; ?>">
				Đặt mua
			</a>
		</div>
	</div>
</div>

<!--popups-->
<?php
    if($type == true) {
?>
<div class="popups hosting-standarrd-<?php echo $post_id; ?>">
    <div class="popups-content hosting-standarrd">
        <div class="popup-box">
            <div class="content hosting-standarrd">
                <a href="javascript:void(0)" title="" class="close" data_close="hosting-standarrd-<?php echo $post_id; ?>">
                    <i class="icofont-close icon"></i>
                </a>
                <h3 class="title">Chọn thời gian cho gói</h3>
                <div class="groups-box">

                	<!--show variations-->
					<?php
						$variations_info = $product_info->get_available_variations();

						$i = 1;
						foreach($variations_info as $variations_kq) {
							$variation_id 			= $variations_kq["variation_id"];
							$variation_price 		= $variations_kq["display_price"];
							$variation_name_check 	= wc_get_product($variation_id)->get_formatted_name();
							$variation_name 		= str_replace( array("".$post_title." - ", " (#".$variation_id.")"), array(" ", " "), $variation_name_check );
						
							// $variation = new WC_Product_Variation($variation_id);
							// echo $variationName = implode(" / ", $variation->get_variation_attributes());
					?>
	                    <div class="item">
	                        <div class="content-box">
	                            <h4><?php echo $variation_name; ?></h4>

	                            <p class="price">
	                            	<?php if( $i==1 ) { ?>

		                            	<!--Nếu có Theme trong giỏ thì cho mua hosting FREE-->
		                            	<?php if(empty( $check_product_theme )) { ?>
		                            		Tổng: <span><?php echo format_price($variation_price); ?></span> <?php echo $donvi; ?>
		                            	<?php } else { ?>
		                            		<span class="d-block">FREE</span>
		                            		<del>Tổng: <span><?php echo format_price($variation_price); ?></span> <?php echo $donvi; ?></del>
		                            	<?php } ?>

		                            <?php } else { ?>
		                            		Tổng: <span><?php echo format_price($variation_price); ?></span> <?php echo $donvi; ?>
		                            <?php } ?>
	                            </p>

	                            <div class="button-box">

		                            <?php if(!empty( $variation_price )) { ?>
									<form class="variations_form cart" action="<?php echo wc_get_cart_url(); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post_id; ?>" data-product_variations="">
									<!-- <form class="variations_form cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $post_link ) ); ?>" method="post" enctype='multipart/form-data' data-product_id="<?php echo $post_id; ?>" data-product_variations=""> -->
										<div class="single_variation_wrap">
											<div class="woocommerce-variation-add-to-cart variations_button">

												<button type="submit" class="single_add_to_cart_button button alt">Chọn mua</button>

												<input type="hidden" name="add-to-cart" value="<?php echo $post_id; ?>" />
												<input type="hidden" name="product_id" value="<?php echo $post_id; ?>" />
												<input type="hidden" name="variation_id" class="variation_id" value="<?php echo $variation_id; ?>" />
											</div>
										</div>
									</form>
									<?php } ?>

								</div>

	                        </div>
	                    </div>
	                <?php $i++; } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
