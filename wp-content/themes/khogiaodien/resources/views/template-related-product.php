<?php
	global $post;
	$terms 		= get_the_terms( $post->ID , 'product_cat', 'string');
	$term_ids 	= wp_list_pluck($terms,'term_id');
	
	$query = new WP_Query( array(
		'post_type' 	 => 'product',
		'tax_query' 	 => array(
			array(
				'taxonomy' 	=> 'product_cat',
				'field' 	=> 'id',
				'terms' 	=> $term_ids,
				'operator'	=> 'IN'
			 )),
		'posts_per_page' => -1,
		'orderby' 		 => 'date',
		'post__not_in'	 => array($post->ID)
	) );
?>


<article class="lth-products">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="module module_products">                            
                    <div class="module_title">
                        <h3 class="title">Giao diện tương tự khác</h3>
<!--                         <div class="button-box">
                            <a href="san-pham.php" title="" class="">
                                Xem tất cả
                                <i class="icofont-arrow-right icon"></i>
                            </a>
                        </div> -->
                    </div>
                    <div class="module_content">
                        <div class="slick-slider slick-products">

							<?php
								if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
							?>

								<?php get_template_part('resources/views/content/related-product', get_post_format()); ?>

							<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>