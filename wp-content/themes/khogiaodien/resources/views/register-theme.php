<?php
    //field
    $h_form_title   = get_field('h_form_title', 'option');
    $h_form_id      = get_field('h_form', 'option');
    $h_form         = do_shortcode('[contact-form-7 id="'.$h_form_id.'"]');
?>

<div class="button-box">
    <a href="javascript:void(0)" title="" class="btn btn-popup" data_popup="registration">
        <?php echo $h_form_title; ?>
    </a>
</div>

<div class="popups registration">
    <div class="popups-content">
        <div class="popup-box">
            <div class="content">
                <a href="javascript:void(0)" title="" class="close" data_close="registration">
                    <i class="icofont-close icon"></i>
                </a>

                <?php if(!empty( $h_form )) { ?>
                <div class='contacts-form'>
                    <?php echo $h_form; ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>