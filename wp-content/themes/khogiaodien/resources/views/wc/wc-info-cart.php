<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="cart-mini">
    <a href="<?php echo $cart_page_url; ?>" title="" class="">
        <i class="icofont-shopping-cart icon"></i>
        <span class="number">
            <?php if($cart_count > 0) { echo $cart_count; } else { echo 0; } ?>
        </span>
    </a>
</div>