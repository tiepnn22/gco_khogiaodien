<?php
    if(function_exists('wp_nav_menu')){
        $args = array(
            'theme_location' 	=> 	'primary',
            'container'         =>  'div',         // bao ngoài
            'container_class'   =>  'menu-main-menu-container',
            'container_id'      =>  'menu-main-menu-container',
            // 'menu_class'		=>	'menu',        // class ul
            'items_wrap'        =>  '<ul id="menu-main-menu" class="nav-menu">%3$s</ul>'     // thay đổi html ul
        );
        wp_nav_menu( $args );
    }
?>



<!--Phải để trong này vì nó hiện trong menu mobile nữa-->
<?php get_template_part("resources/views/register-theme"); ?>