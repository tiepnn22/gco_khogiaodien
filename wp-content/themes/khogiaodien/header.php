<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<!--mobile-->
<div class="megamenu megamenu-mobile-content d-xl-none">
    <div class="content-box">
        <a class="close-title"><i class="icofont-close"></i></a>

        <?php get_template_part("resources/views/menu"); ?>
    </div>                             
</div>
<header class="headers">
    <div class="header-main">
        <div class="header-stick">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 col-column column-1">
                        <div class="header-main-box">
                            <!-- logo -->
                            <?php get_template_part("resources/views/logo"); ?>

                            <div class="groups-box">
                                <div class="megamenu megamenu-destop d-none d-lg-block">
                                    <!-- menu -->
                                    <?php get_template_part("resources/views/menu"); ?>
                                </div>

                                <!-- cart -->
                                <?php get_template_part("resources/views/wc/wc-info-cart"); ?>

                                <div class="megamenu megamenu-mobile d-lg-none">
                                    <a href="#" title="" class="menu-title">
                                        <i class="icofont-minus icon"></i>
                                        <i class="icofont-minus icon"></i>
                                        <i class="icofont-minus icon"></i>
                                    </a>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
