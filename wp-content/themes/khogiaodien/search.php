<?php get_header(); ?>

<?php
	$s = $_GET['s'];
?>

<main class="main-site main-page page-san-pham">
    <article class="lth-searchs style-1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module_searchs">
                        <div class="module_title">
                            <h3 class="title">Tìm kiếm cho : [<?php echo $s; ?>]</h3>
                        </div>

                        <div class="module_content">
                            <?php get_template_part("resources/views/search-form"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <article class="lth-products style-1">
        <div class="container">
            <div class="row">

                <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                    <aside class="lth-sidebars sticky-top">
                        <div class="sidebars">
                            <div class="sidebar sidebar-categories">
                                <h3>Lĩnh vực</h3>

                                <div class="content">
                                    <ul>
                                        <?php
                                            // đọc danh mục cấp 1
                                            $terms_info = get_terms( 'product_cat', array(
                                                'parent'       => 0,
                                                'hide_empty'   => true
                                            ) );

                                            foreach ( $terms_info as $foreach_kq ) {
                                                $f_term_id    = $foreach_kq->term_id;
                                                $f_term_name  = $foreach_kq->name;
                                                $f_term_link  = esc_url(get_term_link($f_term_id));

                                                if($f_term_id != 22) {
                                            ?>
                                                    <li><a href="<?php echo $f_term_link; ?>"><?php echo $f_term_name; ?></a></li>
                                            <?php
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>

                <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                    <div class="module module_products module_products_list">
                        <div class="module_content">
                            <div class="groups-box">
                                
                                <?php
                                    $query = query_search_post_only_taxonomy_paged($s, 'product', 'hosting_cat', 3);
                                    $max_num_pages = $query->max_num_pages;

                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                ?>

                                    <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

                            </div>

                            <!--pagination-->
                            <?php echo paginationCustom( $max_num_pages ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>

<?php get_footer(); ?>