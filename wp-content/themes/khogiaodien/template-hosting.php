<?php
	/*
	Template Name: Mẫu Bảng giá hosting
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $hosting_select_cat = get_field('hosting_select_cat');
?>

<main class="main-site main-page page-gio-hang">		
	<article class="lth-hosting-price">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_hosting_price">
	                    <div class="module_title">
	                    	<h1 class="title"><?php echo $page_name; ?></h1>
	                    </div>
						
						<?php if(!empty( $hosting_select_cat )) { ?>
	                    <div class="module_tab_title">
	                		<ul>

								<?php
									$i=1;
									foreach ($hosting_select_cat as $foreach_kq) {

									$term_id 		= $foreach_kq->term_id;
									$taxonomy_slug 	= $foreach_kq->taxonomy;
									$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
								?>
		                            <li>
		                                <a href="#" title="<?php echo $term_name; ?>"  class="<?php if($i == 1){ echo 'active'; } ?>" data_tab="tab-<?php echo $i;?>">
		                                	<?php echo $term_name; ?>
		                            	</a>
		                            </li>
								<?php $i++; } ?>

	                		</ul>
	                	</div>
	                    <div class="module_content module_tab_content">

							<?php
								$i=1;
								foreach ($hosting_select_cat as $foreach_kq) {
								
								$term_id 		= $foreach_kq->term_id;
								$taxonomy_slug 	= $foreach_kq->taxonomy;
								$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
							?>

	                		<div class="tab-panel tab-<?php echo $i;?> <?php if($i == 1){ echo 'active'; } ?>">
		                		<div class="groups-box">

									<?php
										$query = query_post_by_taxonomy('product', $taxonomy_slug, $term_id, -1);
										if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
									?>
										
										<?php get_template_part('resources/views/content/hosting-product-tab', get_post_format()); ?>

									<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

		                		</div>
	                		</div>

	                		<?php $i++; } ?>

	                	</div>
	                	<?php } ?>

	                </div>
	            </div>
	        </div>
	    </div>
	</article>
</main>

<?php get_footer(); ?>