<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()
?>

<main class="main-site main-page page-san-pham">
    <article class="lth-searchs style-1">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module_searchs">
                        <div class="module_title">
                            <h3 class="title"><?php echo $page_name; ?></h3>
                        </div>

                        <div class="module_content <?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>

<?php get_footer(); ?>