<?php get_header(); ?>

<?php
    //field
    $home_banner_image        	= get_field('home_banner_image');
    $home_banner_title         	= get_field('home_banner_title');
    $home_banner_button_one  	= get_field('home_banner_button_one');
    $home_banner_button_one_url = get_field('home_banner_button_one_url');

    $home_benefit_title 	= get_field('home_benefit_title');
    $home_benefit_content 	= get_field('home_benefit_content');

    $home_web_free_title 	= get_field('home_web_free_title');
    $home_web_free_image 	= get_field('home_web_free_image');
    $home_web_free_url 		= get_field('home_web_free_url');
    $home_web_free_content 	= get_field('home_web_free_content');

    $home_product_title 		= get_field('home_product_title');
    $home_product_select_cat 	= get_field('home_product_select_cat');
    $home_product_all_title		= get_field('home_product_all');
    $home_product_all_url 		= get_field('home_product_all_url');

    $home_register_use_title 	= get_field('home_register_use_title');
    $home_register_use_desc		= get_field('home_register_use_desc');
    $home_register_use_form_id	= get_field('home_register_use_form');
    $home_register_use_form		= do_shortcode('[contact-form-7 id="'.$home_register_use_form_id.'"]');
?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<main class="main-site main-home">		
	<article class="lth-banners style-1">
	    <div class="container-fuild">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_banners">
	                	<div class="module_content">
	                		<div class="content-image">
	                			<a href="javascript:void(0)" title="<?php echo $home_banner_title; ?>" class="image" tabindex="0">
                                    <img src="<?php echo $home_banner_image; ?>" alt="<?php echo $home_banner_title; ?>">
                                </a>
	                		</div>
	                		<div class="content-box">
	                			<p class="text-1"><?php echo $home_banner_title; ?></p>
	                			<div class="button-box">
	                				<a href="<?php echo $home_banner_button_one_url; ?>" title="" class="btn">
	                					<?php echo $home_banner_button_one; ?>
	                				</a>
	                			</div>
	                			<div class="button-box">
	                				<a href="javascript:void(0)" title="" class="btn btn-popup" data_popup="registration">
	                					Đăng ký nhận giao diện
	                				</a>
	                			</div>
	                		</div>
	                	</div>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<article class="lth-features style-1">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_features">
	                	<div class="module_title">
	                		<h3 class="title"><?php echo $home_benefit_title; ?></h3>
	                	</div>

						<?php if(!empty( $home_benefit_content )) { ?>
	                	<div class="module_content">
	                		<div class="slick-slider slick-features">

						        <?php
						            foreach ($home_benefit_content as $foreach_kq) {

						            $post_image = $foreach_kq["image"];
						            $post_title = $foreach_kq["title"];
						            $post_desc  = $foreach_kq["desc"];
						        ?>
									<div class="item">
										<div class="content">
											<div class="content-image">
												<div class="image">
													<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
												</div>
											</div>
											<div class="content-box">
												<h4><?php echo $post_title; ?></h4>
												<p><?php echo $post_desc; ?></p>
											</div>
										</div>
									</div>
								<?php } ?>

	                		</div>
	                	</div>
						<?php } ?>
	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<article class="lth-banners style-2">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="module module_banners">
	                	<div class="module_title">
	                		<h3 class="title"><?php echo $home_web_free_title; ?></h3>
	                	</div>
	                </div>
	            </div>

				<?php if(!empty( $home_web_free_content )) { ?>
	            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
	            	<div class="module module_banners">
	                	<div class="module_content module_content_left">
	                		<div class="content-box">
	                			<ul>
							        <?php
							        	$i = 1;
							            foreach ($home_web_free_content as $foreach_kq) {

							            $post_title = $foreach_kq["title"];
							            if($i <= 4) {
							        ?>
		                				<li><?php echo $post_title; ?></li>
	                				<?php } $i++; } ?>
	                			</ul>
	                		</div>
	                	</div>
	                </div>
	            </div>
				<?php } ?>

	            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_banners">
	                	<div class="module_content">
	                		<div class="content-image">
	                			<a href="<?php echo $home_web_free_url; ?>" title="<?php echo $home_web_free_title; ?>" class="image" tabindex="0">
                                    <img src="<?php echo $home_web_free_image; ?>" alt="<?php echo $home_web_free_title; ?>">
                                </a>
	                		</div>
	                	</div>
	                </div>
	            </div>

				<?php if(!empty( $home_web_free_content )) { ?>
	            <div class="col-xl-3 col-lg-6 col-md-12 col-sm-12 col-12">
	            	<div class="module module_banners">
	                	<div class="module_content module_content_right">
	                		<div class="content-box">
	                			<ul>
							        <?php
							        	$i = 1;
							            foreach ($home_web_free_content as $foreach_kq) {

							            $post_title = $foreach_kq["title"];
							            if($i >= 5) {
							        ?>
		                				<li><?php echo $post_title; ?></li>
	                				<?php } $i++; } ?>
	                			</ul>
	                		</div>
	                	</div>
	                </div>
	            </div>
	            <?php } ?>

	        </div>
	    </div>
	</article>

	<article class="lth-products style-1">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_products">
	                	<div class="module_title">
	                		<h3 class="title"><?php echo $home_product_title; ?></h3>
	                	</div>

	                	<?php if(!empty( $home_product_select_cat )) { ?>
	                	<div class="module_tab_title">
	                		<ul>

								<?php
									$i=1;
									foreach ($home_product_select_cat as $foreach_kq) {

									$term_id 		= $foreach_kq->term_id;
									$taxonomy_slug 	= $foreach_kq->taxonomy;
									$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
								?>
		                            <li>
		                                <a href="#" title="<?php echo $term_name; ?>"  class="<?php if($i == 1){ echo 'active'; } ?>" data_tab="tab-<?php echo $i;?>">
		                                	<?php echo $term_name; ?>
		                            	</a>
		                            </li>
								<?php $i++; } ?>

	                		</ul>
	                	</div>
	                	<div class="module_content module_tab_content">

							<?php
								$i=1;
								foreach ($home_product_select_cat as $foreach_kq) {
								
								$term_id 		= $foreach_kq->term_id;
								$taxonomy_slug 	= $foreach_kq->taxonomy;
								$term_name 		= get_term( $term_id, $taxonomy_slug )->name;
							?>
		                		<div class="tab-panel tab-<?php echo $i;?> <?php if($i == 1){ echo 'active'; } ?>">
			                		<div class="groups-box">

										<?php
											$query = query_post_by_taxonomy('product', $taxonomy_slug, $term_id, 12);
											if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
										?>
											
											<?php get_template_part('resources/views/content/home-product-tab', get_post_format()); ?>

										<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

			                		</div>
		                		</div>
		                	<?php $i++; } ?>

	                		<div class="button-box">
	                			<a href="<?php echo $home_product_all_url; ?>" title="" class="btn"><?php echo $home_product_all_title; ?></a>
	                		</div>
	                	</div>
	                	<?php } ?>

	                </div>
	            </div>
	        </div>
	    </div>
	</article>

	<article class="lth-contacts style-1">
	    <div class="container">
	        <div class="row">
	            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                <div class="module module_contacts">
	                	<div class="module_title">
	                		<h3 class="title"><?php echo $home_register_use_title; ?></h3>
	                		<p class="infor"><?php echo $home_register_use_desc; ?></p>
	                	</div>
	                	<div class="module_content">
                            <?php if(!empty( $home_register_use_form )) { ?>
                            <div class='module_content'>
                                <?php echo $home_register_use_form; ?>
                            </div>
                            <?php } ?>
	                	</div>

	                </div>
	            </div>
	        </div>
	    </div>
	</article>
</main>

<?php get_footer(); ?>

