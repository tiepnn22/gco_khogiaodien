<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
    $product_id = get_the_ID();

    $terms_hosting = wp_get_object_terms($post->ID, 'hosting_cat');
	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id        = $term->term_id;
    $term_name      = $term->name;
    // $term_excerpt   = wpautop($term->description);
    // $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_slug  = $term->taxonomy;
    // $thumbnail_id        = get_term_meta( $term_id, 'thumbnail_id', true ); // woo
    // $term_image_check    = wp_get_attachment_url( $thumbnail_id ); // woo
    // $term_image          = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    //woocommerce
    $product = new WC_product($product_id);

    //gallery
    $single_product_gallery = $product->get_gallery_image_ids();
    
    //info product
    $single_product_title       = get_the_title($product_id);
    $single_product_date        = get_the_date('d/m/Y', $product_id);
    $single_product_link        = get_permalink($product_id);
    $single_product_image       = getPostImage($product_id,"full");
    $single_product_excerpt     = get_the_excerpt($product_id);
    $single_recent_author       = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author      = $single_recent_author->display_name;
    $single_product_tag         = get_the_tags($product_id);

    //field
    $product_demo_url = get_field('product_demo_url', $product_id);
?>

<?php if(empty($terms_hosting)) { ?>
<main class="main-site main-page page-single page-san-pham-chi-tiet">       
    <article class="lth-single lth-product-single">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="module module_single">                          
                        <div class="module_title">
                            <div>
                                <h1 class="title"><?php echo $single_product_title; ?></h1>
                                <p class="infor">Template, giao diện mẫu</p>
                            </div>

                            <?php if(!empty( $product_demo_url )) { ?>
                            <div class="button-box">
                                <a href="<?php echo get_link_page_template('template-demo.php'); ?>?id=<?php echo $product_id; ?>" title="" class="btn">
                                    <i class="icofont-eye-alt"></i>
                                    Xem giao diện
                                </a>
                            </div>
                            <?php } ?>
                            
                        </div>
                        <div class="module_content">
                            <div class="groups-box">
                                <div class="single-image">
                                    <div class="image">
                                        <img src="<?php echo $single_product_image; ?>" alt="<?php echo $single_product_title; ?>">
                                    </div>
                                </div>
                                <div class="single-content">
                                    <h3 class="single-name"><?php echo $term_name; ?></h3>
                                    <div class="single-price">
                                        <?php echo show_price_old_price($product_id); ?>
                                    </div>
                                    <div class="single-button">
                                        <div class="button-box">
                                            <a href="javascript:void(0)" title="" class="btn btn-popup" data_popup="registration">Đăng ký giao diện</a>
                                        </div>
                                        <div class="button-box">
                                            <?php echo show_add_to_cart_button_ajax($product_id); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <?php get_template_part("resources/views/template-related-product"); ?>
</main>
<?php } ?>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
