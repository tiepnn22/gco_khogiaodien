<?php
    //field
    $customer_address        = get_field('customer_address', 'option');
    $customer_phone          = get_field('customer_phone', 'option');
    $customer_email          = get_field('customer_email', 'option');
    $customer_address_child  = get_field('customer_address_child', 'option');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_google     = get_field('f_socical_google', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_zalo       = get_field('f_socical_zalo', 'option');

    $f_bottom_copyright   = get_field('f_bottom_copyright', 'option');
?>

<footer>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">    
                    <div class="footer-main-box">
                        <div class="footer-logo">
                            <!-- logo -->
                            <?php get_template_part("resources/views/logo-footer"); ?>
                        </div>

                        <div class="footer-box footer-contact-box groups-box">
                            <div class="item">
                                <h3>Trụ sở chính</h3>
                                <ul>
                                    <li><?php echo $customer_address; ?></li>
                                    <li><a href="tel: <?php echo str_replace(' ','',$customer_phone);?>" title=""><?php echo $customer_phone; ?></a></li>
                                    <li><a href="mailto: <?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a></li>
                                </ul>
                            </div>

                            <?php if(!empty( $customer_address_child )) { ?>
                            <?php
                                foreach ($customer_address_child as $foreach_kq) {

                                $post_name      = $foreach_kq["name"];
                                $post_address   = $foreach_kq["address"];
                                $post_phone     = $foreach_kq["phone"];
                                $post_email     = $foreach_kq["email"];
                            ?>
                                <div class="item">
                                    <h3><?php echo $post_name; ?></h3>
                                    <ul>
                                        <li><?php echo $post_address; ?></li>
                                        <li><a href="tel: <?php echo str_replace(' ','',$post_phone);?>" title=""><?php echo $post_phone; ?></a></li>
                                        <li><a href="mailto: <?php echo $post_email; ?>" title=""><?php echo $post_email; ?></a></li>
                                    </ul>
                                </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">    
                    <div class="footer-bottom-box">
                        <div class="copyright">
                            <p><?php echo $f_bottom_copyright; ?></p>
                        </div>

                        <div class="social-box">
                            <ul>
                                <li>
                                    <a href="<?php echo $f_socical_facebook; ?>" title="Facebook" target="_blank">
                                        <img src="<?php echo asset('images/icon-06.png'); ?>" alt="Social">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_google; ?>" title="Google +" target="_blank">
                                        <img src="<?php echo asset('images/icon-07.png'); ?>" alt="Social">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_twitter; ?>" title="Twitter" target="_blank">
                                        <img src="<?php echo asset('images/icon-08.png'); ?>" alt="Social">
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_zalo; ?>" title="Zalo" target="_blank">
                                        <img src="<?php echo asset('images/icon-09.png'); ?>" alt="Social">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- <div class="back-to-top">
    <i class="icofont-simple-up icon" aria-hidden="true"></i>
</div> -->

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</body>
</html>